<?php
namespace DreamFactory\Common\Interfaces;

/**
 * Exportable
 * Can export data
 */
interface Exportable
{
	/**
	 * @return array
	 */
	public function export();
}
