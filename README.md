# lib-php-common

Common PHP Components Library for the DreamFactory Services Platform(tm)

This library is a set of components for PHP used by the DreamFactory Services Platform&trade;

# Installation

Add a line to your "require" section in your composer configuration:

	"require":           {
		"dreamfactory/lib-php-common": "*"
	}

Run a composer update:

    $ composer update
